import dotenv from 'dotenv'
dotenv.config()

interface IConfig {
  port: number
  isProduction: boolean
  secretKey: string
  isTestEnvironment: boolean
}

export const initConfig = (): IConfig => {
  const { NODE_ENV, PORT, SECRET_KEY } = process.env
  switch (NODE_ENV) {
    case 'development':
      return {
        isProduction: false,
        isTestEnvironment: false,
        secretKey: SECRET_KEY || 'test',
        port: Number(PORT) || 3001
      }
    case 'production':
      return {
        isProduction: true,
        isTestEnvironment: false,
        secretKey: SECRET_KEY || 'test',
        port: Number(PORT) || 3001
      }
    case 'test':
      return {
        isProduction: false,
        isTestEnvironment: true,
        secretKey: SECRET_KEY || 'test',
        port: Number(PORT) || 4000
      }
    default:
      return {
        isProduction: false,
        isTestEnvironment: false,
        secretKey: SECRET_KEY || 'test',
        port: Number(PORT) || 3001
      }
  }
}

export const config = initConfig();
