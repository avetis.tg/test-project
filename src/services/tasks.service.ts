import {PrismaClient, Task} from '@prisma/client';
import {HttpException} from '../exceptions/HttpException';
import {isEmpty} from 'lodash';
import {CreateTaskDto} from "../dtos/tasks.dto";
// please note I tried to use dto, couldn't figure out and will as one of the team members later

class TaskService {
  public tasks = new PrismaClient().task;

  public async findTasksByUserId(userId: number): Promise<Task[]> {

    const tasks: Task[] | null = await this.tasks.findMany({ where: { ownerId: userId } });

    if (!tasks) throw new HttpException(409, "Task doesn't exist");

    return tasks;

  }

  public async createTask(taskData: any): Promise<Task> {

    if (isEmpty(taskData)) throw new HttpException(400, "Task data is empty");

    return await this.tasks.create({data: taskData});

  }

  public async updateTask(taskId: number, data: any): Promise<Task> {

    if (isEmpty(data)) throw new HttpException(400, "data is empty");

    const findTask: Task | null = await this.tasks.findUnique({ where: { id: taskId } });
    if (!findTask) throw new HttpException(409, "Task doesn't exist");

    return await this.tasks.update({where: {id: taskId}, data});

  }

  public async deleteTask(taskId: number): Promise<Task> {

    if (isEmpty(taskId)) throw new HttpException(400, "Task doesn't existId");

    const findTask: Task | null = await this.tasks.findUnique({ where: { id: taskId } });
    if (!findTask) throw new HttpException(409, "Task doesn't exist");

    return await this.tasks.delete({where: {id: taskId}});

  }
}

export default TaskService;
