import {PrismaClient, User} from '@prisma/client';
import {HttpException} from '../exceptions/HttpException';
import {isEmpty} from 'lodash';

class UserService {
    public users = new PrismaClient().user;

    public async getOrCreateUser(data: { id: number }): Promise<User> {

        if (isEmpty(data)) throw new HttpException(400, "User data is empty");

        const {id} = data;

        const user = await this.users.findUnique({
            where: {
                id
            }
        });

        if (user) {
            return user
        }

        return await this.users.create({data: {id, username: `${Date.now()}`}});

    }
}

export default UserService;
