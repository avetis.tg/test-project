import { sign } from 'jsonwebtoken';
import { PrismaClient, User } from '@prisma/client';
import { config } from '../../config';
import { DataStoredInToken, TokenData } from '../interfaces/auth.interface';

class AuthService {
    public users = new PrismaClient().user;

    public createToken(user: User): TokenData {
        const dataStoredInToken: DataStoredInToken = { id: user.id };
        const secretKey: string = config.secretKey;
        const expiresIn: number = 60 * 60;

        return { expiresIn, token: sign(dataStoredInToken, secretKey, { expiresIn }) };
    }
}

export default AuthService;
