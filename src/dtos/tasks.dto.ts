import { IsString } from 'class-validator';

export class CreateTaskDto {
  @IsString()
  public id: string | undefined;

  @IsString()
  public title: string | undefined;

  @IsString()
  public owner: number | undefined;

  @IsString()
  public ownerId: number | undefined;

  @IsString()
  public notes: number | undefined;

  @IsString()
  public dueDate: number | undefined;

  @IsString()
  public isDone: number | undefined;

  @IsString()
  public createdAt: number | undefined;
}
