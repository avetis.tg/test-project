import { IsEmail } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  public username: string | undefined;

}
