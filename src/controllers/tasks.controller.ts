import { NextFunction, Request, Response } from 'express';
import { Task } from '@prisma/client';
import { CreateTaskDto } from '../dtos/tasks.dto';
import TaskService from '../services/tasks.service';

class TasksController {
  public taskService = new TaskService();

  public getTaskByUserId = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const {userId} = req.params;

      const findAllTasksData: Task[] = await this.taskService.findTasksByUserId(Number(userId));

      res.status(200).json({ data: findAllTasksData, message: 'findAllByUser' });

    } catch (error) {
      next(error);
    }
  };

  public createTask = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const taskData: CreateTaskDto = req.body || {};
      const createTaskData: Task = await this.taskService.createTask(taskData);

      res.status(201).json({ data: createTaskData, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateTask = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const taskId = Number(req.params.id);
      const taskData: CreateTaskDto = req.body;
      const updateTaskData: Task = await this.taskService.updateTask(taskId, taskData);

      res.status(200).json({ data: updateTaskData, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteTask = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const taskId = Number(req.params.id);
      const deleteTaskData: Task = await this.taskService.deleteTask(taskId);

      res.status(200).json({ data: deleteTaskData, message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default TasksController;
