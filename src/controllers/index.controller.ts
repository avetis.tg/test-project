import {NextFunction, Request, Response} from 'express';
import AuthService from '../services/auth.service';
import UserService from '../services/users.service';

class IndexController {
    public index = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const authService = new AuthService();
            const userService = new UserService();
            const {userId} = req.params;
            console.log('userId', userId);

            const user = await userService.getOrCreateUser({id: Number(userId)})

            const {token} = authService.createToken(user);

            res.status(200).json({data: {user, token}, message: 'findAllByUser'});

        } catch (error) {
            next(error);
        }
    };
}

export default IndexController;
