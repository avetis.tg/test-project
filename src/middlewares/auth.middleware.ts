import { NextFunction, Response, Request } from 'express';
import { verify } from 'jsonwebtoken';
import { PrismaClient } from '@prisma/client';
import { config } from '../../config';
import { HttpException } from '../exceptions/HttpException';
import { DataStoredInToken/*, RequestWithUser*/ } from '../interfaces/auth.interface';

const authMiddleware = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    const Authorization = req.cookies['Authorization']
        || (req.header('Authorization')
            ? (req.header('Authorization') || '').split('Bearer ')[1]
            : null);

    if (Authorization) {
      const verificationResponse = (await verify(Authorization, config.secretKey)) as DataStoredInToken;
      const userId = verificationResponse.id;

      const users = new PrismaClient().user;
      const findUser = await users.findUnique({ where: { id: Number(userId) } });

      if (findUser) {
        // todo ask Noro and fix
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        req.user = findUser;
        next();
      } else {
        next(new HttpException(401, 'Wrong authentication token'));
      }
    } else {
      next(new HttpException(404, 'Authentication token missing'));
    }
  } catch (error) {
    next(new HttpException(401, 'Wrong authentication token'));
  }
};

export default authMiddleware;
