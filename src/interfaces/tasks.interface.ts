import { User } from '@prisma/client';

export interface CreateTaskDto {
  title: string;
  owner: User;
  ownerId: number;
  notes: string;
  dueDate: Date;
  isDone: boolean;
  createdAt: Date;
}
