import { Router } from 'express';
import TasksController from '../controllers/tasks.controller';
import { Routes } from '../interfaces/routes.interface';
import {CreateTaskDto} from "../dtos/tasks.dto";
import authMiddleware from '../middlewares/auth.middleware';
import validationMiddleware from '../middlewares/validation.middleware';

class TasksRoute implements Routes {
  public path = '/tasks';
  public router = Router();
  public tasksController = new TasksController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/:userId`, authMiddleware, this.tasksController.getTaskByUserId);
    // todo ask NORO!!!!
    this.router.post(`${this.path}`, authMiddleware, validationMiddleware(CreateTaskDto, 'body'), this.tasksController.createTask);
    this.router.put(`${this.path}/:id`, authMiddleware, validationMiddleware(CreateTaskDto, 'body'), this.tasksController.updateTask);
    this.router.delete(`${this.path}/:id`, authMiddleware, this.tasksController.deleteTask);
  }
}

export default TasksRoute;
