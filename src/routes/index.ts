import TaskRoute from './tasks.route';
import IndexRoute from './index.route';

export const routes = [new TaskRoute(), new IndexRoute()];
