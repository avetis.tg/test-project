[Centralised Services Middleware](../README.md) / [Exports](../modules.md) / user/user.service

# Module: user/user.service

## Table of contents

### Functions

- [create](user_user_service.md#createuser)

## Functions

### create

▸ `Const` **create**(`user`): `Promise`<`User`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `user` | `Object` |
| `user.email` | `string` |
| `user.name` | `string` |

#### Returns

`Promise`<`User`\>

#### Defined in

[user/tasks.service.ts:3](https://github.com/pshaddel/ts-express-prisma-rest/blob/811a292/src/user/user.service.ts#L3)
